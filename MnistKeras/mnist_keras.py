"""Trains using Keras with back propagation a NN of MNIST dataset using the following instructions:
    * MNIST digits classifier
    * First layer (Input layer) with 784 Neurons
    * Create N required hidden layers
    * Last layer (output layer) with 10 neurons (one neuron per digit)
    * test heuristics

    Created by:
        Duran Reynoso Jose Angel
        Parrao Alcantará Manuel Sebastían
        Villaseñor Barragan Jaime

    Notes:
        The NN structure was built like:

    ╔═════╦═════════════╦══════════════════╦══════════════════╦══════════════╗
    ║  N  ║ Input Layer ║ 1st Hidden Layer ║ 2nd Hidden Layer ║ Output Layer ║
    ╠═════╬═════════════╬══════════════════╬══════════════════╬══════════════╣
    ║ 0   ║     0       ║       0          ║        0         ║     0        ║
    ║ 1   ║     1       ║       1          ║        1         ║     1        ║
    ║ 2   ║     2       ║       2          ║        2         ║     2        ║
    ║ 3   ║     3       ║       3          ║        3         ║     3        ║
    ║ .   ║     .       ║       .          ║        .         ║     .        ║
    ║ .   ║     .       ║       .          ║        .         ║     .        ║
    ║ .   ║     .       ║       .          ║        .         ║     .        ║
    ║ 784 ║     784     ║       512        ║        512       ║     9        ║
    ║ ___ ║ ___________ ║ ________________ ║ ________________ ║ ____________ ║
    ║ -   ║     FC      ║       FC         ║        FC        ║     -        ║
    ╚═════╩═════════════╩══════════════════╩══════════════════╩══════════════╝

    Results:
        - With 1 Epochs we can get:
            Loss 0.0928
            Accuracy 0.9714
        - With 10 Epochs we can get:
            Loss 0.09253018434214319
            Accuracy 0.9791
        - With 50 Epochs we can get:
            Loss 0.14305470094014705
            Accuracy 0.9802

"""

# region import region

import matplotlib
# Import libraries to handle arrays and plotting using matplot
import numpy as np

# Prints plotting on png format
matplotlib.use('agg')

# Matplot
import matplotlib.pyplot as plotter

# Setup Keras backend with TensorFlow
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
# For Use Tensorflow with GPU use:
# os.environ['CUDA_VISIBLE_DEVICES'] = ''

# The necessary keras modules (dataset, and modules to build the NN)
from keras.datasets import mnist
from keras.utils import np_utils
from keras.models import Sequential, load_model
from keras.layers.core import Dense, Activation

# endregion

# region data set loading
# loading the data set in split segments (train and test)
(data_set_train_x, data_set_train_y), (data_set_test_x, data_set_test_y) = mnist.load_data()
# -endregion

# region print sample without training
shape = plotter.figure()
for i in range(9):
    plotter.subplot(3, 3, i + 1)
    plotter.tight_layout()
    plotter.imshow(data_set_train_x[i], cmap='gray', interpolation='none')
    plotter.title("Class {}".format(data_set_train_y[i]))
    plotter.xticks([])
    plotter.yticks([])
# shape

filepath = "results/sample_without_training.png"
plotter.savefig(filepath)

# endregion

# region print pixel distribution
# For one sample digit it's printing the pixel distribution to get a simple idea of the pixel average inside the picture
shape = plotter.figure()
plotter.subplot(2, 1, 1)
plotter.imshow(data_set_train_x[0], cmap='gray', interpolation='none')
plotter.title("Class {}".format(data_set_train_y[0]))
plotter.xticks([])
plotter.yticks([])
plotter.subplot(2, 1, 2)
plotter.hist(data_set_train_x[0].reshape(784))
plotter.title("Pixel Value Distribution")
# shape

filepath = "results/sample_pixel_distribution.png"
plotter.savefig(filepath)

# endregion

# region printing shape
# shape data set will be printed to know their structure (necessary to understand the reshaping process)
print("X_train shape", data_set_train_x.shape)
print("y_train shape", data_set_train_y.shape)
print("X_test shape", data_set_test_x.shape)
print("y_test shape", data_set_test_y.shape)
# endregion

# region reshaping data set process
# Instead of having a data set like -> [60000][28][28] we convert the shape to -> [60000][784]
# We flatted the image representation (matrix to vector) and after that, we converted the data type to float32
data_set_train_x = data_set_train_x.reshape(60000, 784)
data_set_test_x = data_set_test_x.reshape(10000, 784)
data_set_train_x = data_set_train_x.astype('float32')
data_set_test_x = data_set_test_x.astype('float32')
# endregion

# region data normalization
# It's necessary to get only the data between 0 and 1 (it's very helpful for the training process)
data_set_train_x /= 255
data_set_test_x /= 255
# endregion

# region reprinting shape
# Print again the shape of data set to verify if is ready train
print("Train matrix shape", data_set_train_x.shape)
print("Test matrix shape", data_set_test_x.shape)

print(np.unique(data_set_train_y, return_counts=True))
# endregion

# region refactoring labels data set
# Convert the Y-labels in one shape compatible with the planed NN output using Keras numpy-related utilities
n_classes = 10
print("Shape before one-hot encoding: ", data_set_train_y.shape)
Y_train = np_utils.to_categorical(data_set_train_y, n_classes)
Y_test = np_utils.to_categorical(data_set_test_y, n_classes)
print("Shape after one-hot encoding: ", Y_train.shape)
# endregion

# region NN structure building
# Using a Sequential model, we will build a Neural Network using a linear stack of layers
neural_network_builder = Sequential()
neural_network_builder.add(Dense(512, input_shape=(784,)))
neural_network_builder.add(Activation('relu'))
# neural_network_builder.add(Dropout(0.2))

neural_network_builder.add(Dense(512))
neural_network_builder.add(Activation('relu'))
# neural_network_builder.add(Dropout(0.2))

neural_network_builder.add(Dense(10))
neural_network_builder.add(Activation('softmax'))
# endregion

# region compiling and training
# We will compile the sequential model defining the metrics, the model optimzer, and the loss functions
neural_network_builder.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer='adam')

# Now, we are training the model and saving metrics in history of the training
history = neural_network_builder.fit(data_set_train_x, Y_train,
                                     batch_size=128,
                                     epochs=50,
                                     verbose=2,
                                     validation_data=(data_set_test_x, Y_test))
# endregion

# region Storing the model
save_dir = "results/"
model_name = 'keras_mnist.h5'
model_path = os.path.join(save_dir, model_name)
neural_network_builder.save(model_path)
print('Saved trained model at %s ' % model_path)
# endregion

# region Plotting the metrics obtained during the training process
shape = plotter.figure()
plotter.subplot(2, 1, 1)
plotter.plot(history.history['acc'])
plotter.plot(history.history['val_acc'])
plotter.title('model accuracy')
plotter.ylabel('accuracy')
plotter.xlabel('epoch')
plotter.legend(['train', 'test'], loc='lower right')

plotter.subplot(2, 1, 2)
plotter.plot(history.history['loss'])
plotter.plot(history.history['val_loss'])
plotter.title('model loss')
plotter.ylabel('loss')
plotter.xlabel('epoch')
plotter.legend(['train', 'test'], loc='upper right')

plotter.tight_layout()

filepath = "results/metrics_of_training.png"
plotter.savefig(filepath)

# shape
# endregion

# region Verification of model
# we will load the model and test the accuracy creating predictions
mnist_model = load_model("results/keras_mnist.h5")
predicted_classes = mnist_model.predict_classes(data_set_test_x)

loss_and_metrics = mnist_model.evaluate(data_set_test_x, Y_test, verbose=2)

print("Test Loss", loss_and_metrics[0])
print("Test Accuracy", loss_and_metrics[1])

# endregion

# region Printing statistics of the classification
# see which we predicted correctly and which not
correct_indices = np.nonzero(predicted_classes == data_set_test_y)[0]
incorrect_indices = np.nonzero(predicted_classes != data_set_test_y)[0]
print()
print(len(correct_indices), " classified correctly")
print(len(incorrect_indices), " classified incorrectly")
# endregion

# region printing correct and incorrect classifications
# adapt figure size to show 18 subplots (9 correct classifications and 9 incorrect classifications)
# note: the first 9 figures will be are correct classifications and the rest will be incorrect
plotter.rcParams['figure.figsize'] = (7, 14)

figure_evaluation = plotter.figure()

# plot 9 correct predictions
for i, correct in enumerate(correct_indices[:9]):
    plotter.subplot(6, 3, i + 1)
    plotter.imshow(data_set_test_x[correct].reshape(28, 28), cmap='gray', interpolation='none')
    plotter.title("Predicted {}, Class {}".format(predicted_classes[correct], data_set_test_y[correct]))
    plotter.xticks([])
    plotter.yticks([])

# plot 9 incorrect predictions
for i, incorrect in enumerate(incorrect_indices[:9]):
    plotter.subplot(6, 3, i + 10)
    plotter.imshow(data_set_test_x[incorrect].reshape(28, 28), cmap='gray', interpolation='none')
    plotter.title("Predicted {}, Class {}".format(predicted_classes[incorrect], data_set_test_y[incorrect]))
    plotter.xticks([])
    plotter.yticks([])

filepath = "results/correct_and_incorrect_classifications.png"
plotter.savefig(filepath)

# figure_evaluation
# endregion
